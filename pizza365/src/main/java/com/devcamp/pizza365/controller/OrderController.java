package com.devcamp.pizza365.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.pizza365.model.COrder;
import com.devcamp.pizza365.model.CUser;
import com.devcamp.pizza365.repository.IOrderRepository;
import com.devcamp.pizza365.repository.IUserRepository;
import com.devcamp.pizza365.service.UserService;
import com.devcamp.pizza365.service.OrderService;

@RestController
@CrossOrigin
@RequestMapping("/v1/order")
public class OrderController {
    @Autowired
    private UserService userService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private IOrderRepository pIOrderRepository;
    @Autowired
    private IUserRepository pIUserRepository;

    // get all order list
    @GetMapping("/all")
    public ResponseEntity<List<COrder>> getAllOrdersApi() {
        try {
            return new ResponseEntity<>(orderService.getAllOrders(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    // get order by user id
    @GetMapping("/search/userId={id}")
    public ResponseEntity<Set<COrder>> getOrderByUserIdApi(@PathVariable(name = "id") long id) {
        try {
            Set<COrder> userOrders = userService.getOrderByUserId(id);
            if (userOrders != null) {
                return new ResponseEntity<>(userOrders, HttpStatus.OK);
            } else
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // get order by id
    @GetMapping("/detail/{id}")
    public COrder getOrderById(@PathVariable(name = "id") Long id) {
        if (pIOrderRepository.findById(id).isPresent())
            return pIOrderRepository.findById(id).get();
        else
            return null;
    }

    // create new ward with district id
    @PostMapping("/create/{id}")
    public ResponseEntity<Object> createWard(@PathVariable("id") int id, @RequestBody COrder pOrder) {
        try {
            CUser user = pIUserRepository.findById(id);
            if (user != null) {
                COrder newOrder = new COrder();
                newOrder.setOrderCode(pOrder.getOrderCode());
                newOrder.setPizzaSize(pOrder.getPizzaSize());
                newOrder.setPizzaType(pOrder.getPizzaType());
                newOrder.setVoucherCode(pOrder.getVoucherCode());
                newOrder.setUser(user);
                newOrder.setPrice(pOrder.getPrice());
                newOrder.setPaid(pOrder.getPaid());
                newOrder.setCreated(new Date());
                newOrder.setUpdated(null);

                COrder _order = pIOrderRepository.save(newOrder);
                return new ResponseEntity<>(_order, HttpStatus.CREATED);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // create order
    @PostMapping("/create")
    public ResponseEntity<Object> createOrder(@RequestBody COrder newOrder) {
        COrder _order = new COrder();
        try {
            _order.setOrderCode(newOrder.getOrderCode());
            _order.setPizzaSize(newOrder.getPizzaSize());
            _order.setPizzaType(newOrder.getPizzaType());
            _order.setVoucherCode(newOrder.getVoucherCode());
            _order.setUser(newOrder.getUser());
            _order.setPrice(newOrder.getPrice());
            _order.setPaid(newOrder.getPaid());
            _order.setCreated(new Date());
            _order.setUpdated(null);

            COrder savedRole = pIOrderRepository.save(_order);
            return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Order: " + e.getCause().getMessage());
        }
    }

    // update order by id
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateOrder(@PathVariable(name = "id") Long id, @RequestBody COrder updateOrder) {
        Optional<COrder> _orderData = pIOrderRepository.findById(id);
        if (_orderData.isPresent()) {
            COrder _order = _orderData.get();
            _order.setPizzaSize(updateOrder.getPizzaSize());
            _order.setPizzaType(updateOrder.getPizzaType());
            _order.setVoucherCode(updateOrder.getVoucherCode());
            _order.setPrice(updateOrder.getPrice());
            _order.setPaid(updateOrder.getPaid());
            _order.setUpdated(new Date());
            try {
                return ResponseEntity.ok(pIOrderRepository.save(_order));
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Can not excute operation of this Entity: " + e.getCause().getMessage());
            }
        } else {
            return new ResponseEntity<Object>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // delete user by Id
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<COrder> deleteOrderById(@PathVariable("id") long id) {
        try {
            pIOrderRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
