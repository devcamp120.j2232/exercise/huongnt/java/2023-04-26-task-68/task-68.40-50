package com.devcamp.pizza365.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.pizza365.model.CRegion;
import com.devcamp.pizza365.model.District;
import com.devcamp.pizza365.repository.IDistrictRepository;
import com.devcamp.pizza365.repository.RegionRepository;
import com.devcamp.pizza365.service.DistrictService;
import com.devcamp.pizza365.service.RegionService;

@RestController
@RequestMapping("/district")
@CrossOrigin
public class DistrictController {
  @Autowired
  DistrictService districtService;
  @Autowired
  IDistrictRepository pIDistrictRepository;
  @Autowired
  RegionService pRegionService;
  @Autowired
  RegionRepository pRegionRepository;

  @GetMapping("/all")
  public ResponseEntity<List<District>> getAllDistricts() {
    try {
      return new ResponseEntity<>(districtService.getAllDistricts(),
          HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }
  }

  // get district by region id
  @GetMapping("/search/regionId={id}")
  public ResponseEntity<Set<District>> getDistrictByRegionId(@PathVariable(name = "id") int id) {
    try {
      Set<District> vDistrict = pRegionService.getDistrictByRegionId(id);
      if (vDistrict != null) {
        return new ResponseEntity<>(vDistrict, HttpStatus.OK);
      } else {
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
      }
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  // get district by id
  @GetMapping("/detail/{id}")
  public ResponseEntity<Object> getDistrictById(@PathVariable(name = "id", required = true) Integer id) {
    Optional<District> district = pIDistrictRepository.findById(id);
    if (district.isPresent()) {
      return new ResponseEntity<>(district, HttpStatus.OK);
    } else {
      return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }
  }

  // create new district by province
  @PostMapping("/create/{id}")
  public ResponseEntity<Object> createDistrict(@PathVariable("id") int id, @RequestBody District pDistrict) {
    try {
      CRegion region = pRegionRepository.findById(id);
      if (region != null) {
        District newDistrict = new District();
        newDistrict.setNameDistrict(pDistrict.getNameDistrict());
        newDistrict.setPrefixDistrict(pDistrict.getPrefixDistrict());
        newDistrict.setRegion(region);
        District _district = pIDistrictRepository.save(newDistrict);
        // District _district = districtRepository.save(pDistrict);
        return new ResponseEntity<>(_district, HttpStatus.CREATED);
      } else
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

    } catch (Exception e) {
      System.out.println(e);
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  // update distict
  @PutMapping("/update/{id}")
  public ResponseEntity<Object> updateDistrict(@PathVariable(name = "id") Integer id,
      @RequestBody District updateDistrict) {
    Optional<District> district = pIDistrictRepository.findById(id);
    try {
      if (district.isPresent()) {
        District _district = district.get();
        _district.setNameDistrict(updateDistrict.getNameDistrict());
        _district.setPrefixDistrict(updateDistrict.getPrefixDistrict());
        _district.setRegion(updateDistrict.getRegion());

        return new ResponseEntity<>(pIDistrictRepository.save(_district),
            HttpStatus.OK);
      } else {
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
      }

    } catch (Exception e) {
      System.out.println(e);
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  // delete district by Id
  @DeleteMapping("/delete/{id}")
  public ResponseEntity<District> deleteDistrictById(@PathVariable("id") Integer id) {
    try {
      pIDistrictRepository.deleteById(id);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      System.out.println(e);
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

}
